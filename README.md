# Sneaky Malloc

We intercept calls to malloc, write the data to disk, so that it can later be visualized.

This is a seed for a memory profiler. It produces visuals that are very cool.

Here is a memory visualization of Google chrome's main process:

<img src="Sneakymalloc_Visualizing_memory_allocations_made_by_google_chrome_s_main_process.gif">