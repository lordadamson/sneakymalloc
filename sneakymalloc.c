#define _GNU_SOURCE
#include <stdio.h>
#include <dlfcn.h>
#include <stdlib.h>
#include <stddef.h>
#include <unistd.h>
#include <string.h>
#include <fcntl.h>
#include <stdint.h>
#include <stdbool.h>
#include <pthread.h>

#define UNW_LOCAL_ONLY
#include <libunwind.h>

// Branch prediction hints
// stolen from valgrind source
#if defined(__GNUC__)
#  define LIKELY(x)   __builtin_expect(!!(x), 1)
#  define UNLIKELY(x) __builtin_expect(!!(x), 0)
#else
#  define LIKELY(x)   (x)
#  define UNLIKELY(x) (x)
#endif

static const char* log_path = "/tmp/log";
static int log_fd = -1;

static const char* lock_path = "/tmp/sneakymalloc_lock";
static int lock_fd = -1;
static bool should_operate = true;

static pthread_mutex_t mutex;

#include <sys/stat.h>

bool
file_exists(const char* filename)
{
	struct stat st;
	return (stat (filename, &st) == 0);
}

/// 8 for the pointer where the allocation happened
/// 8 for the size of the allocation
/// 800 for the stacktrace: 8 bytes * a maximum of a hundred pc addresses
#define max_entry_size 816

/// that's the value I'll put to tell that this is a deletion
#define deletion 0xffffffffffffffff

typedef void* (*original_malloc_t)(size_t);
typedef void* (*original_realloc_t)(void*, size_t);
typedef void* (*original_calloc_t)(size_t, size_t);
typedef void  (*original_free_t)(void*);

static inline void
log_file_init()
{
	pthread_mutex_lock(&mutex);
	if(log_fd == -1)
	{
		write(1, "initializing log file\n", strlen("initializing log file\n"));
		truncate(log_path, 0);
		log_fd = open(log_path, O_CREAT | O_RDWR, (mode_t)0700);

		if(log_fd == -1)
		{
			abort();
		}
	}

	pthread_mutex_unlock(&mutex);
}

/// The constructor is invoked in the parent process
/// and when execv is called
static void __attribute__((constructor))
constructor(void)
{	
	if(should_operate == false)
	{
		return;
	}

	if(file_exists(lock_path))
	{
		should_operate = false;
		return;
	}

	lock_fd = open(lock_path, O_CREAT | O_RDWR, (mode_t)0700);

	if(lock_fd == -1)
	{
		abort();
	}

	should_operate = true;

	log_file_init();
}

static void __attribute__((destructor))
destructor(void)
{
	if(should_operate == false)
	{
		return;
	}

	remove(lock_path);
}

void
sneaky_log(void* ptr, uint64_t size)
{
	if(should_operate == false)
	{
		return;
	}

	unw_cursor_t    cursor;
	unw_context_t   context;

	unw_getcontext(&context);
	unw_init_local(&cursor, &context);

	size_t stack_trace_size = 0;
	char stack_trace[800];
	while (unw_step(&cursor) > 0)
	{
		unw_word_t  offset, pc;


		unw_get_reg(&cursor, UNW_REG_IP, &pc);

		unw_get_proc_name(&cursor, stack_trace + stack_trace_size,
						  sizeof(stack_trace) - stack_trace_size, &offset);

		stack_trace_size = strlen(stack_trace) + 1;
		stack_trace[stack_trace_size - 1] = '\n';
	}

	stack_trace[stack_trace_size - 1] = '\0';

	// the 24 are
	//   - 8 bytes: pointer location
	//   - 8 bytes: allocation size
	//   - 8 bytes: size of stack trace.
	// Then the actual stack trace.
	size_t entry_size = 24 + stack_trace_size;

	size_t offset = 0;
	char to_write[max_entry_size];

	memcpy(to_write, &ptr, sizeof(ptr));
	offset = sizeof(ptr);

	memcpy(to_write + offset, &size, sizeof(size));
	offset += sizeof(size);

	memcpy(to_write + offset, &stack_trace_size, sizeof(uint64_t));
	offset += sizeof(uint64_t);

	strcpy(to_write + offset, stack_trace);

	pthread_mutex_lock(&mutex);
	write(log_fd, to_write, entry_size);
	pthread_mutex_unlock(&mutex);
}

void
free(void* ptr)
{
	original_free_t original_free =
			(original_free_t)dlsym(RTLD_NEXT, "free");

	if(UNLIKELY(!original_free))
	{
		abort();
	}

	original_free(ptr);

	if(ptr == NULL)
	{
		return;
	}

	sneaky_log(ptr, deletion);
}

void*
realloc(void* old_ptr, size_t size)
{
	original_realloc_t original_realloc =
			(original_realloc_t)dlsym(RTLD_NEXT, "realloc");

	if(UNLIKELY(!original_realloc))
	{
		abort();
	}

	void* output = (*original_realloc)(old_ptr, size);

	if(!output)
	{
		return output;
	}

	if(old_ptr == NULL)
	{
		return output;
	}

	sneaky_log(old_ptr, deletion);
	sneaky_log(output, size);

	return output;
}

void* calloc(size_t el_count, size_t el_size)
{
	// In glibc, dlsym() calls calloc. Calling dlsym(RTLD_NEXT, "calloc") here
	// causes infinite recursion.  Instead, we simply use it by its other
	// name.
	// This trick was stolen from the libunwind source code. Thanks guys.
	extern void *__libc_calloc(size_t, size_t);
	original_calloc_t original_calloc = __libc_calloc;

	if(UNLIKELY(!original_calloc))
	{
		abort();
	}

	void* output = (*original_calloc)(el_count, el_size);

	if(!output)
	{
		return output;
	}

	if(el_count == 0 || el_size == 0)
	{
		return output;
	}

	size_t size = el_size * el_count;

	sneaky_log(output, size);

	return output;
}

void* malloc(size_t size)
{
	original_malloc_t original_malloc =
			(original_malloc_t)dlsym(RTLD_NEXT, "malloc");

	if(UNLIKELY(!original_malloc))
    {
	    abort();
    }

    void* output = (*original_malloc)(size);

    if(!output)
    {
	    return output;
    }

	if(size == 0)
	{
		return output;
	}

	sneaky_log(output, size);

	return output;
}
