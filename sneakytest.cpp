#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <sys/wait.h>

int
main()
{
	malloc(1000);

	int pid = fork();

	if(pid == 0)
	{
		malloc(1000);
		return 0;
	}

	pid = fork();

	if(pid == 0)
	{
		malloc(3000);
		return 0;
	}

	malloc(2000);
	wait(NULL);
}
