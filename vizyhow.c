#define STB_IMAGE_WRITE_IMPLEMENTATION
#define STB_IMAGE_RESIZE_IMPLEMENTATION
#include "stb_image_write.h"
#include "stb_image_resize.h"

#include <math.h>
#include <fcntl.h>
#include <stdint.h>
#include <unistd.h>
#include <stdbool.h>
#include<sys/wait.h>
#include <sys/stat.h>
#include <sys/types.h>

#define MAX_PATH_LEN 1024

static uint64_t
vizy_abs(int64_t n)
{
	return n * ((n>0) - (n<0));
}

struct Image
{
	uint8_t* data;
	uint32_t h;
	uint32_t w;
	uint32_t c;
};

struct Color
{
	uint8_t r;
	uint8_t g;
	uint8_t b;
	uint8_t a;
};

static const struct Color colors[4] =
{
	{249,253,254,255},
	{250,157,28,255},
	{248,88,89,255},
	{50,179,226,255}
};

static struct Color
color_randomize()
{
	return colors[rand() % 4];
}

static bool
color_equal(struct Color c1, struct Color c2)
{
	return
	c1.r == c2.r &&
	c1.g == c2.g &&
	c1.b == c2.b &&
	c1.a == c2.a;
}

struct Color
color_from_pixel(uint8_t* pixel, uint8_t channels)
{
	if(channels == 1)
	{
		struct Color c = {
			.r = *pixel,
			.g = *pixel,
			.b = *pixel,
			.a = 255,
		};

		return c;
	}

	if(channels == 3)
	{
		struct Color c = {
			.r = pixel[0],
			.g = pixel[1],
			.b = pixel[2],
			.a = 255,
		};

		return c;
	}

	struct Color color = {
		.r = pixel[0],
		.g = pixel[1],
		.b = pixel[2],
		.a = pixel[3],
	};

	return color;
}

static struct Image
image_make(uint32_t width, uint32_t height, uint32_t channels)
{
	struct Image img = {
		.data = calloc(height * width * channels, 1),
		.h = height,
		.w = width,
		.c = channels
	};

	if(img.data == NULL)
	{
		abort();
	}

	return img;
}

static void
image_write(struct Image img, const char* path)
{
	int success = stbi_write_png(path, img.w, img.h, img.c, img.data, 0);

	if(!success)
	{
		fprintf(stderr, "Couldn't write an image\n");
		abort();
	}
}

static struct Image
image_resize(struct Image img, float resize_ratio)
{
	struct Image resized = {
		.w = img.w * resize_ratio,
		.h = img.h * resize_ratio,
		.c = img.c,
	};

	resized.data = malloc(resized.w * resized.h * resized.c);

	int success = stbir_resize_uint8(img.data, img.w, img.h, 0,
					   resized.data, resized.w, resized.h, 0, img.c);

	if(!success)
	{
		fprintf(stderr, "Couldn't resize an image\n");
		abort();
	}

	return resized;
}

static void
image_free(struct Image image)
{
	free(image.data);
}

static uint8_t*
image_pixel(struct Image img, uint32_t x, uint32_t y)
{
	return &img.data[img.c * (img.w * y + x)];
}

static struct Color
image_pixel_color(struct Image img, uint32_t x, uint32_t y)
{
	uint8_t* pixel = image_pixel(img, x, y);

	return color_from_pixel(pixel, img.c);
}

static struct Image
image_blacken_until_color_change(struct Image img, uint32_t x, uint32_t y)
{
	struct Color original_color = image_pixel_color(img, x, y);
	uint8_t* last_pixel = image_pixel(img, img.w, img.h);
	uint8_t* pixel = image_pixel(img, x, y);

	for(; pixel != last_pixel; pixel += img.c)
	{
		struct Color pixel_color = color_from_pixel(pixel, img.c);

		if(!color_equal(original_color, pixel_color))
		{
			return img;
		}

		memset(pixel, 0, img.c);
	}

	return img;
}

static struct Image
image_set_color_until(struct Image img, uint32_t x, uint32_t y,
					  struct Color c, uint32_t until)
{
	uint8_t color[4] = {c.r, c.g, c.b, c.a};
	uint8_t* pixel = image_pixel(img, x, y);

	while(until --> 0)
	{
		for(size_t k = 0; k < img.c; k++)
		{
			pixel[k] = color[k];
		}

		pixel += img.c;
	}

	return img;
}

static struct Image
image_concatenate(struct Image img1, struct Image img2)
{
	if(img1.h != img2.h)
	{
		fprintf(stderr, "can't concatinate two images side by side that "
						"have different heights\n");
		abort();
	}

	struct Image output = {
		.w = img1.w + img2.w,
		.h = img1.h,
		.c = img1.c
	};

	output.data = malloc(output.w * output.h * output.c);

	size_t output_row_size = output.w * output.c;
	size_t img1_row_size = img1.w * img1.c;
	size_t img2_row_size = img2.w * img2.c;

	for(size_t y = 0; y < output.h; y++)
	{
		memcpy(output.data + output_row_size * y, img1.data + img1_row_size * y, img1_row_size);
		memcpy(output.data + output_row_size * y + img1_row_size, img2.data + img2_row_size * y, img2_row_size);
	}

	return output;
}

#define ascii_height 18

/// height is always 18
struct ASCIIChar
{
	const char* data;
	size_t w;
};

static const struct ASCIIChar ascii_chars[128] = {
	{0},{0},{0},{0},{0},{0},{0},{0},{0},{0},{0},{0},{0},{0},{0},{0},{0},{0},{0},
	{0},{0},{0},{0},{0},{0},{0},{0},{0},{0},{0},{0},{0},
	{ // 32
		.data =
			"1111111"
			"1111111"
			"1111111"
			"1111111"
			"1111111"
			"1111111"
			"1111111"
			"1111111"
			"1111111"
			"1111111"
			"1111111"
			"1111111"
			"1111111"
			"1111111"
			"1111111"
			"1111111"
			"1111111"
			"1111111",
		.w = 7
	},
	{0},{0},{0},{0},{0},{0},{0},{0},{0},{0},{0},{0},{0},{0},{0},
	{ // 48
		.data =
			"11111111111"
			"11000001111"
			"10000000111"
			"10011100111"
			"00011100011"
			"00111110011"
			"00111110011"
			"00111110011"
			"00111110011"
			"00111110011"
			"00011100011"
			"10011100111"
			"10000000111"
			"11000001111"
			"11111111111"
			"11111111111"
			"11111111111"
			"11111111111",
		.w = 11
	},
	{
		.data =
			"1111111111"
			"1000011111"
			"0000011111"
			"0010011111"
			"1110011111"
			"1110011111"
			"1110011111"
			"1110011111"
			"1110011111"
			"1110011111"
			"1110011111"
			"1110011111"
			"0000000011"
			"0000000011"
			"1111111111"
			"1111111111"
			"1111111111"
			"1111111111",
		.w = 10
	},
	{
		.data =
			"1111111111"
			"1000001111"
			"0000000111"
			"0111100011"
			"1111110011"
			"1111110011"
			"1111100011"
			"1111100111"
			"1111001111"
			"1100011111"
			"1000111111"
			"0000111111"
			"0000000011"
			"0000000011"
			"1111111111"
			"1111111111"
			"1111111111"
			"1111111111",
		.w = 10
	},
	{
		.data =
			"11111111111"
			"10000001111"
			"00000000111"
			"00111100011"
			"11111110011"
			"11111100011"
			"11000000111"
			"11000000111"
			"11111100011"
			"11111110011"
			"11111110011"
			"01111100011"
			"00000000111"
			"10000001111"
			"11111111111"
			"11111111111"
			"11111111111"
			"11111111111",
		.w = 11
	},
	{
		.data =
			"111111111111"
			"111110001111"
			"111110001111"
			"111100001111"
			"111001001111"
			"110011001111"
			"110011001111"
			"100111001111"
			"001111001111"
			"000000000011"
			"000000000011"
			"111111001111"
			"111111001111"
			"111111001111"
			"111111111111"
			"111111111111"
			"111111111111"
			"111111111111",
		.w = 12
	},
	{
		.data =
			"1111111111"
			"1000000111"
			"1000000111"
			"1001111111"
			"1001111111"
			"1000001111"
			"1000000111"
			"1011100011"
			"1111110011"
			"1111110011"
			"1111110011"
			"0111100011"
			"0000000111"
			"1000001111"
			"1111111111"
			"1111111111"
			"1111111111"
			"1111111111",
		.w = 10
	},
	{
		.data =
			"11111111111"
			"11100001111"
			"11000000111"
			"10001110111"
			"00011111111"
			"00011111111"
			"00100001111"
			"00000000111"
			"00011100011"
			"00111110011"
			"00111110011"
			"10011100011"
			"10000000111"
			"11000001111"
			"11111111111"
			"11111111111"
			"11111111111"
			"11111111111",
		.w = 11
	},
	{
		.data =
			"1111111111"
			"0000000011"
			"0000000011"
			"1111100011"
			"1111100111"
			"1111100111"
			"1111000111"
			"1111001111"
			"1111001111"
			"1110001111"
			"1110011111"
			"1110011111"
			"1100011111"
			"1100111111"
			"1111111111"
			"1111111111"
			"1111111111"
			"1111111111",
		.w = 10
	},
	{
		.data =
			"11111111111"
			"11000001111"
			"00000000011"
			"00011100011"
			"00111110011"
			"00011100011"
			"10000000111"
			"10000000111"
			"00011100011"
			"00111110011"
			"00111110011"
			"00011100011"
			"10000000111"
			"11000001111"
			"11111111111"
			"11111111111"
			"11111111111"
			"11111111111",
		.w = 11
	},
	{
		.data =
			"11111111111"
			"11000001111"
			"10000000111"
			"00011100111"
			"00111110011"
			"00111110011"
			"00011100011"
			"10000000011"
			"11000010011"
			"11111100011"
			"11111100011"
			"10111000111"
			"10000001111"
			"11000011111"
			"11111111111"
			"11111111111"
			"11111111111"
			"11111111111",
		.w = 11
	},
	{0},{0},{0},{0},{0},{0},{0},
	{ // 65
		.data =
			"11111111111111"
			"11111001111111"
			"11111001111111"
			"11110000111111"
			"11110000111111"
			"11100110011111"
			"11100110011111"
			"11100110011111"
			"11001111001111"
			"11000000001111"
			"10000000000111"
			"10011111100111"
			"10011111100111"
			"00111111110011"
			"11111111111111"
			"11111111111111"
			"11111111111111"
			"11111111111111",
		.w = 14
	},
	{
		.data =
			"11111111111"
			"00000001111"
			"00000000111"
			"00111000111"
			"00111100111"
			"00111000111"
			"00000001111"
			"00000001111"
			"00111100111"
			"00111110011"
			"00111110011"
			"00111100011"
			"00000000111"
			"00000001111"
			"11111111111"
			"11111111111"
			"11111111111"
			"11111111111",
		.w = 11
	},
	{
		.data =
			"1111111111111"
			"1111000000111"
			"1100000000011"
			"1000011111011"
			"1001111111111"
			"0011111111111"
			"0011111111111"
			"0011111111111"
			"0011111111111"
			"0011111111111"
			"1001111111111"
			"1000111111011"
			"1100000000011"
			"1111000000111"
			"1111111111111"
			"1111111111111"
			"1111111111111"
			"1111111111111",
		.w = 13
	},
	{
		.data =
			"1111111111111"
			"0000000011111"
			"0000000001111"
			"0011111000111"
			"0011111100011"
			"0011111110011"
			"0011111110011"
			"0011111110011"
			"0011111110011"
			"0011111110011"
			"0011111100011"
			"0011111000111"
			"0000000001111"
			"0000000011111"
			"1111111111111"
			"1111111111111"
			"1111111111111"
			"1111111111111",
		.w = 13
	},
	{
		.data =
			"1111111111"
			"0000000011"
			"0000000011"
			"0011111111"
			"0011111111"
			"0011111111"
			"0000000011"
			"0000000011"
			"0011111111"
			"0011111111"
			"0011111111"
			"0011111111"
			"0000000011"
			"0000000011"
			"1111111111"
			"1111111111"
			"1111111111"
			"1111111111",
		.w = 10
	},
	{
		.data =
			"1111111111"
			"0000000011"
			"0000000011"
			"0011111111"
			"0011111111"
			"0011111111"
			"0000000111"
			"0000000111"
			"0011111111"
			"0011111111"
			"0011111111"
			"0011111111"
			"0011111111"
			"0011111111"
			"1111111111"
			"1111111111"
			"1111111111"
			"1111111111",
		.w = 10
	},
	{
		.data =
			"1111111111111"
			"1110000000111"
			"1100000000011"
			"1000111110011"
			"0001111111111"
			"0011111111111"
			"0011110000011"
			"0011110000011"
			"0011111110011"
			"0011111110011"
			"0001111110011"
			"1000111100011"
			"1100000000011"
			"1110000001111"
			"1111111111111"
			"1111111111111"
			"1111111111111"
			"1111111111111",
		.w = 13
	},
	{
		.data =
			"111111111111"
			"001111110011"
			"001111110011"
			"001111110011"
			"001111110011"
			"001111110011"
			"000000000011"
			"000000000011"
			"001111110011"
			"001111110011"
			"001111110011"
			"001111110011"
			"001111110011"
			"001111110011"
			"111111111111"
			"111111111111"
			"111111111111"
			"111111111111",
		.w = 12
	},
	{
		.data =
			"1111"
			"0011"
			"0011"
			"0011"
			"0011"
			"0011"
			"0011"
			"0011"
			"0011"
			"0011"
			"0011"
			"0011"
			"0011"
			"0011"
			"1111"
			"1111"
			"1111"
			"1111",
		.w = 4
	},
	{
		.data =
			"1111111"
			"1110011"
			"1110011"
			"1110011"
			"1110011"
			"1110011"
			"1110011"
			"1110011"
			"1110011"
			"1110011"
			"1110011"
			"1110011"
			"1110011"
			"1110011"
			"1110011"
			"1100011"
			"0000111"
			"0001111",
		.w = 7
	},
	{
		.data =
			"111111111111"
			"001111100011"
			"001111000111"
			"001110011111"
			"001100111111"
			"001001111111"
			"000011111111"
			"000011111111"
			"001001111111"
			"001100111111"
			"001110011111"
			"001111001111"
			"001111000111"
			"001111100011"
			"111111111111"
			"111111111111"
			"111111111111"
			"111111111111",
		.w = 12
	},
	{
		.data =
			"1111111111"
			"0011111111"
			"0011111111"
			"0011111111"
			"0011111111"
			"0011111111"
			"0011111111"
			"0011111111"
			"0011111111"
			"0011111111"
			"0011111111"
			"0011111111"
			"0000000011"
			"0000000011"
			"1111111111"
			"1111111111"
			"1111111111"
			"1111111111",
		.w = 10
	},
	{
		.data =
			"11111111111111"
			"00011111100011"
			"00001111000011"
			"00001111000011"
			"00100110010011"
			"00100110010011"
			"00100110010011"
			"00110000110011"
			"00110000110011"
			"00111001110011"
			"00111001110011"
			"00111111110011"
			"00111111110011"
			"00111111110011"
			"11111111111111"
			"11111111111111"
			"11111111111111"
			"11111111111111",
		.w = 14
	},
	{
		.data =
			"111111111111"
			"000111110011"
			"000111110011"
			"000011110011"
			"000011110011"
			"001001110011"
			"001000110011"
			"001100110011"
			"001100010011"
			"001110010011"
			"001111000011"
			"001111000011"
			"001111100011"
			"001111100011"
			"111111111111"
			"111111111111"
			"111111111111"
			"111111111111",
		.w = 12
	},
	{
		.data =
			"11111111111111"
			"11100000011111"
			"11000000001111"
			"10001111000111"
			"00011111100011"
			"00111111110011"
			"00111111110011"
			"00111111110011"
			"00111111110011"
			"00111111110011"
			"00011111100011"
			"10001111000111"
			"11000000001111"
			"11100000011111"
			"11111111111111"
			"11111111111111"
			"11111111111111"
			"11111111111111",
		.w = 14
	},
	{
		.data =
			"1111111111"
			"0000001111"
			"0000000111"
			"0011100011"
			"0011110011"
			"0011110011"
			"0011100011"
			"0000000111"
			"0000001111"
			"0011111111"
			"0011111111"
			"0011111111"
			"0011111111"
			"0011111111"
			"1111111111"
			"1111111111"
			"1111111111"
			"1111111111",
		.w = 10
	},
	{
		.data =
			"11111111111111"
			"11100000011111"
			"11000000001111"
			"10001111000111"
			"00011111100011"
			"00111111110011"
			"00111111110011"
			"00111111110011"
			"00111111110011"
			"00111111110011"
			"00011111100111"
			"10001111000111"
			"11000000001111"
			"11100000011111"
			"11111110011111"
			"11111110001111"
			"11111111111111"
			"11111111111111",
		.w = 14
	},
	{
		.data =
			"111111111111"
			"000000111111"
			"000000011111"
			"001110001111"
			"001111001111"
			"001111001111"
			"001110001111"
			"000000011111"
			"000000111111"
			"001110011111"
			"001110001111"
			"001111000111"
			"001111100111"
			"001111100011"
			"111111111111"
			"111111111111"
			"111111111111"
			"111111111111",
		.w = 12
	},
	{
		.data =
			"11111111111"
			"11000001111"
			"10000000111"
			"00011110111"
			"00111111111"
			"00011111111"
			"10000011111"
			"11000000111"
			"11111000011"
			"11111110011"
			"11111110011"
			"00111100011"
			"00000000111"
			"10000001111"
			"11111111111"
			"11111111111"
			"11111111111"
			"11111111111",
		.w = 11
	},
	{
		.data =
			"11111111111111"
			"00000000000011"
			"00000000000011"
			"11111001111111"
			"11111001111111"
			"11111001111111"
			"11111001111111"
			"11111001111111"
			"11111001111111"
			"11111001111111"
			"11111001111111"
			"11111001111111"
			"11111001111111"
			"11111001111111"
			"11111111111111"
			"11111111111111"
			"11111111111111"
			"11111111111111",
		.w = 14
	},
	{
		.data =
			"111111111111"
			"001111110011"
			"001111110011"
			"001111110011"
			"001111110011"
			"001111110011"
			"001111110011"
			"001111110011"
			"001111110011"
			"001111110011"
			"001111110011"
			"000111100011"
			"100000000111"
			"110000001111"
			"111111111111"
			"111111111111"
			"111111111111"
			"111111111111",
		.w = 12
	},
	{
		.data =
			"11111111111111"
			"00111111110011"
			"00011111100011"
			"10011111100111"
			"10001111000111"
			"11001111001111"
			"11001111001111"
			"11000110001111"
			"11100110011111"
			"11100000011111"
			"11110000111111"
			"11110000111111"
			"11110000111111"
			"11111001111111"
			"11111111111111"
			"11111111111111"
			"11111111111111"
			"11111111111111",
		.w = 14
	},
	{
		.data =
			"1111111111111111111"
			"0011111000111110011"
			"0001110000011100011"
			"1001110000011100111"
			"1001110010011100111"
			"1001110010011100111"
			"1000100010001000111"
			"1100100111001001111"
			"1100100111001001111"
			"1100000111001001111"
			"1100000111000001111"
			"1110001111100011111"
			"1110001111100011111"
			"1110001111100011111"
			"1111111111111111111"
			"1111111111111111111"
			"1111111111111111111"
			"1111111111111111111",
		.w = 19
	},
	{
		.data =
			"1111111111111"
			"1000111100011"
			"1100111000111"
			"1100011001111"
			"1110000001111"
			"1111000011111"
			"1111000011111"
			"1111000111111"
			"1111000011111"
			"1110000011111"
			"1100010001111"
			"1100111000111"
			"1000111100111"
			"0001111100011"
			"1111111111111"
			"1111111111111"
			"1111111111111"
			"1111111111111",
		.w = 13
	},
	{
		.data =
			"11111111111111"
			"00011111100011"
			"10001111000111"
			"11001111001111"
			"11000110001111"
			"11100000011111"
			"11110000111111"
			"11111001111111"
			"11111001111111"
			"11111001111111"
			"11111001111111"
			"11111001111111"
			"11111001111111"
			"11111001111111"
			"11111111111111"
			"11111111111111"
			"11111111111111"
			"11111111111111",
		.w = 14
	},
	{
		.data =
			"1111111111111"
			"0000000000011"
			"0000000000011"
			"1111111000111"
			"1111110001111"
			"1111110011111"
			"1111100011111"
			"1111000111111"
			"1110001111111"
			"1100011111111"
			"1100111111111"
			"1000111111111"
			"0000000000011"
			"0000000000011"
			"1111111111111"
			"1111111111111"
			"1111111111111"
			"1111111111111",
		.w = 13
	},
	{0},{0},{0},{0},{0},{0},
	{ // 97
		.data =
			"1111111111"
			"1111111111"
			"1111111111"
			"1111111111"
			"1100001111"
			"1000000111"
			"1011100011"
			"1111110011"
			"1000000011"
			"0000000011"
			"0011110011"
			"0011100011"
			"0000000011"
			"1000010011"
			"1111111111"
			"1111111111"
			"1111111111"
			"1111111111",
		.w = 10
	},
	{
		.data =
			"00111111111"
			"00111111111"
			"00111111111"
			"00111111111"
			"00100001111"
			"00000000111"
			"00011100011"
			"00111110011"
			"00111110011"
			"00111110011"
			"00111110011"
			"00011100011"
			"00000000111"
			"00100001111"
			"11111111111"
			"11111111111"
			"11111111111"
			"11111111111",
		.w = 11
	},
	{
		.data =
			"1111111111"
			"1111111111"
			"1111111111"
			"1111111111"
			"1100000111"
			"1000000011"
			"0000111011"
			"0011111111"
			"0011111111"
			"0011111111"
			"0011111111"
			"0000111011"
			"1000000011"
			"1100000111"
			"1111111111"
			"1111111111"
			"1111111111"
			"1111111111",
		.w = 10
	},
	{
		.data =
			"11111110011"
			"11111110011"
			"11111110011"
			"11111110011"
			"11000010011"
			"10000000011"
			"00011100011"
			"00111110011"
			"00111110011"
			"00111110011"
			"00111110011"
			"00011100011"
			"10000000011"
			"11000010011"
			"11111111111"
			"11111111111"
			"11111111111"
			"11111111111",
		.w = 11
	},
	{
		.data =
			"111111111111"
			"111111111111"
			"111111111111"
			"111111111111"
			"111000001111"
			"100000000111"
			"100111100011"
			"001111110011"
			"000000000011"
			"000000000011"
			"001111111111"
			"100111111011"
			"100000000011"
			"111000000111"
			"111111111111"
			"111111111111"
			"111111111111"
			"111111111111",
		.w = 12
	},
	{
		.data =
			"111000011"
			"110000011"
			"110011111"
			"110011111"
			"000000011"
			"000000011"
			"110011111"
			"110011111"
			"110011111"
			"110011111"
			"110011111"
			"110011111"
			"110011111"
			"110011111"
			"111111111"
			"111111111"
			"111111111"
			"111111111",
		.w = 9
	},
	{
		.data =
			"11111111111"
			"11111111111"
			"11111111111"
			"11111111111"
			"11000010011"
			"10000000011"
			"00011100011"
			"00111110011"
			"00111110011"
			"00111110011"
			"00111110011"
			"00011100011"
			"10000000011"
			"11000010011"
			"11111110011"
			"10111100011"
			"10000000111"
			"11000001111",
		.w = 11
	},
	{
		.data =
			"0011111111"
			"0011111111"
			"0011111111"
			"0011111111"
			"0010000111"
			"0000000011"
			"0001100011"
			"0011110011"
			"0011110011"
			"0011110011"
			"0011110011"
			"0011110011"
			"0011110011"
			"0011110011"
			"1111111111"
			"1111111111"
			"1111111111"
			"1111111111",
		.w = 10
	},
	{
		.data =
			"0011"
			"0011"
			"1111"
			"1111"
			"0011"
			"0011"
			"0011"
			"0011"
			"0011"
			"0011"
			"0011"
			"0011"
			"0011"
			"0011"
			"1111"
			"1111"
			"1111"
			"1111",
		.w = 4
	},
	{
		.data =
			"110011"
			"110011"
			"111111"
			"111111"
			"110011"
			"110011"
			"110011"
			"110011"
			"110011"
			"110011"
			"110011"
			"110011"
			"110011"
			"110011"
			"110011"
			"100011"
			"000011"
			"000111",
		.w = 6
	},
	{
		.data =
			"00111111111"
			"00111111111"
			"00111111111"
			"00111111111"
			"00111100011"
			"00111001111"
			"00110011111"
			"00100111111"
			"00001111111"
			"00001111111"
			"00000111111"
			"00100011111"
			"00110000111"
			"00111000011"
			"11111111111"
			"11111111111"
			"11111111111"
			"11111111111",
		.w = 11
	},
	{
		.data =
			"0011"
			"0011"
			"0011"
			"0011"
			"0011"
			"0011"
			"0011"
			"0011"
			"0011"
			"0011"
			"0011"
			"0011"
			"0011"
			"0011"
			"1111"
			"1111"
			"1111"
			"1111",
		.w = 4
	},
	{
		.data =
			"1111111111111111"
			"1111111111111111"
			"1111111111111111"
			"1111111111111111"
			"0010000110000111"
			"0000000100000111"
			"0001100001100011"
			"0011110011110011"
			"0011110011110011"
			"0011110011110011"
			"0011110011110011"
			"0011110011110011"
			"0011110011110011"
			"0011110011110011"
			"1111111111111111"
			"1111111111111111"
			"1111111111111111"
			"1111111111111111",
		.w = 16
	},
	{
		.data =
			"1111111111"
			"1111111111"
			"1111111111"
			"1111111111"
			"0010000111"
			"0000000011"
			"0001100011"
			"0011110011"
			"0011110011"
			"0011110011"
			"0011110011"
			"0011110011"
			"0011110011"
			"0011110011"
			"1111111111"
			"1111111111"
			"1111111111"
			"1111111111",
		.w = 10
	},
	{
		.data =
			"111111111111"
			"111111111111"
			"111111111111"
			"111111111111"
			"110000001111"
			"100000000111"
			"000111000011"
			"001111110011"
			"001111110011"
			"001111110011"
			"001111110011"
			"000111100011"
			"100000000111"
			"110000001111"
			"111111111111"
			"111111111111"
			"111111111111"
			"111111111111",
		.w = 12
	},
	{
		.data =
			"11111111111"
			"11111111111"
			"11111111111"
			"11111111111"
			"00100001111"
			"00000000111"
			"00011100011"
			"00111110011"
			"00111110011"
			"00111110011"
			"00111110011"
			"00011100011"
			"00000000111"
			"00100001111"
			"00111111111"
			"00111111111"
			"00111111111"
			"00111111111",
		.w = 11
	},
	{
		.data =
			"11111111111"
			"11111111111"
			"11111111111"
			"11111111111"
			"11000010011"
			"10000000011"
			"00011100011"
			"00111110011"
			"00111110011"
			"00111110011"
			"00111110011"
			"00011100011"
			"10000000011"
			"11000010011"
			"11111110011"
			"11111110011"
			"11111110011"
			"11111110011",
		.w = 11
	},
	{
		.data =
			"11111111"
			"11111111"
			"11111111"
			"11111111"
			"00100011"
			"00000011"
			"00011111"
			"00111111"
			"00111111"
			"00111111"
			"00111111"
			"00111111"
			"00111111"
			"00111111"
			"11111111"
			"11111111"
			"11111111"
			"11111111",
		.w = 8
	},
	{
		.data =
			"111111111"
			"111111111"
			"111111111"
			"111111111"
			"100000111"
			"000000011"
			"000111011"
			"000011111"
			"100000111"
			"111000011"
			"111110011"
			"011100011"
			"000000011"
			"100000111"
			"111111111"
			"111111111"
			"111111111"
			"111111111",
		.w = 9
	},
	{
		.data =
			"11111111"
			"10011111"
			"10011111"
			"10011111"
			"00000011"
			"00000011"
			"10011111"
			"10011111"
			"10011111"
			"10011111"
			"10011111"
			"10011111"
			"10000011"
			"11000011"
			"11111111"
			"11111111"
			"11111111"
			"11111111",
		.w = 8
	},
	{
		.data =
			"1111111111"
			"1111111111"
			"1111111111"
			"1111111111"
			"0011110011"
			"0011110011"
			"0011110011"
			"0011110011"
			"0011110011"
			"0011110011"
			"0011110011"
			"0001100011"
			"0000000011"
			"1000010011"
			"1111111111"
			"1111111111"
			"1111111111"
			"1111111111",
		.w = 10
	},
	{
		.data =
			"111111111111"
			"111111111111"
			"111111111111"
			"111111111111"
			"001111110011"
			"000111100011"
			"100111100111"
			"100111100111"
			"100011000111"
			"110011001111"
			"110010001111"
			"111000011111"
			"111000011111"
			"111000011111"
			"111111111111"
			"111111111111"
			"111111111111"
			"111111111111",
		.w = 12
	},
	{
		.data =
			"111111111111111"
			"111111111111111"
			"111111111111111"
			"111111111111111"
			"001110001110011"
			"001110001110011"
			"000110001100011"
			"100100100100111"
			"100100100100111"
			"100100100100111"
			"100001110000111"
			"110001110001111"
			"110001110001111"
			"110001110001111"
			"111111111111111"
			"111111111111111"
			"111111111111111"
			"111111111111111",
		.w = 15
	},
	{
		.data =
			"111111111111"
			"111111111111"
			"111111111111"
			"111111111111"
			"000111100011"
			"100111100111"
			"110011001111"
			"110000001111"
			"111000011111"
			"111000011111"
			"110000001111"
			"110011001111"
			"100111100111"
			"000111100011"
			"111111111111"
			"111111111111"
			"111111111111"
			"111111111111",
		.w = 12
	},
	{
		.data =
			"111111111111"
			"111111111111"
			"111111111111"
			"111111111111"
			"001111110011"
			"000111100011"
			"100111100111"
			"100011100111"
			"110011001111"
			"110011001111"
			"111000001111"
			"111000011111"
			"111100011111"
			"111100111111"
			"111000111111"
			"111001111111"
			"100001111111"
			"100011111111",
		.w = 12
	},
	{
		.data =
			"1111111111"
			"1111111111"
			"1111111111"
			"1111111111"
			"0000000011"
			"0000000011"
			"1111100111"
			"1111000111"
			"1110001111"
			"1100011111"
			"1000111111"
			"1000111111"
			"0000000011"
			"0000000011"
			"1111111111"
			"1111111111"
			"1111111111"
			"1111111111",
		.w = 10
	},
	{0},{0},{0},{0},{0}
};

static size_t
text_calculate_width(const char* text)
{
	size_t max_width = 0;
	size_t w = 0;

	for(size_t i = 0; i < strlen(text); i++)
	{
		if(text[i] == '\n')
		{
			if(w > max_width)
			{
				max_width = w;
			}

			w = 0;

			continue;
		}

		w += ascii_chars[(size_t)text[i]].w;
	}

	return max_width;
}

static size_t
text_line_numbers(const char* text)
{
	if(strlen(text) == 0)
	{
		return 0;
	}

	size_t lines = 1;

	for(size_t i = 0; i < strlen(text); i++)
	{
		if(text[i] == '\n')
		{
			lines++;
		}
	}

	return lines;
}

static struct Image
text_make_overlay(const char* text)
{
	size_t width = text_calculate_width(text);
	size_t lines_count = text_line_numbers(text);

	size_t text_len = strlen(text);

	struct Image img = {
		.w = width,
		.h = lines_count * ascii_height,
		.c = 1,
	};

	img.data = calloc(img.w * img.h, img.c);

	char* mytext = malloc(text_len + 1); // +1 for null terminator
	strcpy(mytext, text);
	char* mytext_begin = mytext; // will use to free mytext later

	for(size_t i = 0; i < text_len; i++)
	{
		if(mytext[i] == '\n')
		{
			mytext[i] = '\0';
		}
	}

	for(size_t l = 0; l < lines_count; l++)
	{
		size_t line_length = strlen(mytext);
		size_t img_y = l * ascii_height;

		for(size_t y = 0; y < ascii_height; y++)
		{
			uint64_t x = 0;

			for(size_t c = 0; c < line_length; c++)
			{
				size_t char_width = ascii_chars[(size_t)mytext[c]].w;

				memcpy(x + img.data + img.w * img_y,
					   ascii_chars[(size_t)mytext[c]].data + char_width * y,
					   char_width);

				x += char_width;
			}

			img_y++;
		}

		mytext += line_length + 1;
	}

	free(mytext_begin);

	return img;
}

static struct Image
text_render_to_image(const char* text, size_t font_size,
					 struct Color c, size_t x, size_t y,
					 struct Image img)
{
	struct Image text_overlay = text_make_overlay(text);

	size_t lines_count = text_line_numbers(text);

	struct Image resized_overlay = image_resize(text_overlay,
												font_size/((float)text_overlay.h/lines_count));

	image_free(text_overlay);

	for(size_t j = 0; j < resized_overlay.h; j++)
	{
		for(size_t i = 0; i < resized_overlay.w; i++)
		{
			uint8_t* op = image_pixel(resized_overlay, i, j);

			if(*op == '0')
			{
				uint8_t* ip = image_pixel(img, x + i, y + j);
				uint8_t color[4] = {c.r, c.g, c.b, c.a};
				memcpy(ip, color, img.c);
			}
			else if(*op == '1') // one is background
			{
				uint8_t* ip = image_pixel(img, x + i, y + j);
				memset(ip, 0, img.c);
			}
		}
	}

	image_write(img, "/home/adam/img.png");

	image_free(resized_overlay);

	return img;
}

static size_t
text_estimate_width(const char* text, size_t font_size)
{
	float ratio = (float)font_size / ascii_height;
	size_t w = text_calculate_width(text);
	return (size_t)ceilf(w * ratio);
}

struct LogEntry
{
	uint64_t address;
	uint64_t size;
	uint64_t region;
	uint64_t address_offset;
	char stacktrace[800];
};

struct Region
{
	uint64_t id;
	uint64_t max_addr;
};

struct Logs
{
	struct LogEntry* entries;
	size_t entries_count;
	struct Region* regions;
	size_t regions_count;
};

struct LogsParseResult
{
	struct LogEntry* entries;
	size_t entries_count;
};

struct LogsParseContext
{
	size_t idx;
	size_t file_size;
	const char* file;
	size_t entry_idx;
	struct LogsParseResult result;
};

static bool
eat_uint64_t(struct LogsParseContext* ctx, uint64_t* result)
{
	if(ctx->idx + sizeof(uint64_t) >= ctx->file_size)
	{
		return false;
	}

	uint64_t* tmp = (uint64_t*)(ctx->file + ctx->idx);
	*result = *tmp;

	ctx->idx += sizeof(uint64_t);

	return true;
}

static bool
eat_buffer(struct LogsParseContext* ctx, char* buffer, size_t how_much_to_eat)
{
	if(ctx->idx + how_much_to_eat > ctx->file_size)
	{
		return false;
	}

	memcpy(buffer, ctx->file + ctx->idx, how_much_to_eat);

	ctx->idx += how_much_to_eat;

	return true;
}

static size_t
logs_parse_entries_count(struct LogsParseContext ctx)
{
	size_t entries_count = 0;

	while(ctx.idx < ctx.file_size)
	{
		char buffer[800];
		uint64_t stacktrace_size;

		if(!eat_uint64_t(&ctx, &stacktrace_size)) // address
		{
			break;
		}

		if(!eat_uint64_t(&ctx, &stacktrace_size)) // size
		{
			abort();
		}

		if(!eat_uint64_t(&ctx, &stacktrace_size)) // stacktrace_size
		{
			abort();
		}

		if(!eat_buffer(&ctx, buffer, stacktrace_size)) // stacktrace
		{
			abort();
		}

		entries_count++;
	}

	return entries_count;
}

static void
logs_parse_entry(struct LogsParseContext* ctx)
{
	if(!eat_uint64_t(ctx, &ctx->result.entries[ctx->entry_idx].address))
	{
		abort();
	}

	if(!eat_uint64_t(ctx, &ctx->result.entries[ctx->entry_idx].size))
	{
		abort();
	}

	uint64_t stacktrace_size;
	if(!eat_uint64_t(ctx, &stacktrace_size))
	{
		abort();
	}

	if(!eat_buffer(ctx, ctx->result.entries[ctx->entry_idx].stacktrace, stacktrace_size))
	{
		abort();
	}
}

static struct LogsParseResult
logs_parse(const char* file, size_t file_size)
{
	struct LogsParseContext ctx = {
		.idx = 0,
		.file_size = file_size,
		.file = file,
		.entry_idx = 0
	};

	ctx.result.entries_count = logs_parse_entries_count(ctx);
	ctx.result.entries = calloc(ctx.result.entries_count, sizeof(struct LogEntry));

	for(; ctx.entry_idx < ctx.result.entries_count; ctx.entry_idx++)
	{
		logs_parse_entry(&ctx);
	}

	return ctx.result;
}

static struct Logs
logs_read(const char* path)
{
	int fd = open(path, O_RDONLY);

	if(fd == -1)
	{
		fprintf(stderr, "can't open file %s\n", path);
		abort();
	}

	struct stat st;
	memset(&st, 0, sizeof (st));

	fstat(fd, &st);

	char* file = malloc(st.st_size);

	ssize_t file_size = read(fd, file, st.st_size);

	if(file_size != st.st_size)
	{
		fprintf(stderr, "can't read file %s\n", path);
		abort();
	}

	close(fd);

	struct LogsParseResult result = logs_parse(file, file_size);
	free(file);

	struct Logs logs = {
		.entries_count = result.entries_count,
		.entries = result.entries,
		.regions = calloc(result.entries_count, sizeof(struct Region)),
		.regions_count = result.entries_count
	};

	return logs;
}

static void
logs_free(struct Logs logs)
{
	free(logs.entries);
	free(logs.regions);
}

#define deletion 0xffffffffffffffff

static uint64_t
logs_max_allocation(struct Logs logs)
{
	uint64_t max = 0;

	for(size_t i = 0; i < logs.entries_count; i++)
	{
		if(logs.entries[i].size != deletion && logs.entries[i].size > max)
		{
			max = logs.entries[i].size;
		}
	}

	return max;
}



static struct Logs
logs_cluster_allocations(struct Logs logs, uint64_t max_allocation)
{

	for(size_t i = 0; i < logs.entries_count; i++)
	{
		for(size_t j = 0; j < logs.entries_count; j++)
		{
			if(i == j)
			{
				continue;
			}

			if(vizy_abs(logs.entries[i].address - logs.entries[j].address)
			   <= max_allocation * 2)
			{
				logs.entries[j].region = logs.entries[i].region;
			}
			else if(logs.entries[j].region == logs.entries[i].region)
			{
				logs.entries[j].region++;
			}
		}
	}

	uint64_t regions_count = 0;

	for(size_t i = 0; i < logs.entries_count; i++)
	{
		if(regions_count < logs.entries[i].region)
		{
			regions_count = logs.entries[i].region;
		}
	}

	logs.regions_count = regions_count + 1;

	return logs;
}

static struct Logs
logs_normalize_regions(struct Logs logs)
{
	for(size_t r = 0; r < logs.regions_count; r++)
	{
		uint64_t min = 0xffffffffffffffff;
		uint64_t max = 0;

		for(size_t i = 0; i < logs.entries_count; i++)
		{
			if(logs.entries[i].region != r)
			{
				continue;
			}

			struct LogEntry entry = logs.entries[i];

			if(entry.size != deletion && entry.address + entry.size > max)
			{
				max = entry.address + entry.size;
			}

			if(entry.address < min)
			{
				min = entry.address;
			}
		}

		logs.regions[r].max_addr = max - min;

		for(size_t i = 0; i < logs.entries_count; i++)
		{
			if(logs.entries[i].region != r)
			{
				continue;
			}

			logs.entries[i].address -= min;
		}
	}

	return logs;
}

static struct Logs
logs_make_offsets(struct Logs logs)
{
	uint64_t offset = 0;

	for(size_t r = 0; r < logs.regions_count; r++)
	{
		uint64_t max_address = 0;

		for(size_t i = 0; i < logs.entries_count; i++)
		{
			if(logs.entries[i].region != r)
			{
				continue;
			}

			if(logs.entries[i].address > max_address)
			{
				max_address = logs.entries[i].address;
			}

			logs.entries[i].address_offset = offset;
		}

		offset += max_address;
	}

	return logs;
}

/// Will shift the addresses to have them start from zero
/// And will return the maximum address
static struct Logs
logs_make_regions(struct Logs logs)
{
	uint64_t max_allocation = logs_max_allocation(logs);
	logs = logs_cluster_allocations(logs, max_allocation);
	logs = logs_normalize_regions(logs);
	return logs_make_offsets(logs);
}

static uint64_t
nearest_square(uint64_t n)
{
	n = sqrtf((double)n);
	n = ceil(n) + 1;
	n = n * n;
	return (uint64_t)n;
}

static void
mem_address_to_image_coord(uint64_t addr, uint32_t w,  uint32_t* x, uint32_t* y)
{
	*y = addr / w;
	*x = addr % w;
}

static size_t
stacktrace_max_width(struct Logs logs)
{
	size_t max_width = 0;
	for(size_t i = 0; i < logs.entries_count; i++)
	{
		struct LogEntry entry = logs.entries[i];
		char text_to_render[1024];

		if(entry.size == deletion)
		{
			snprintf(text_to_render, 1024, "Stacktrace:\n"
										   "%s", entry.stacktrace);
		}
		else
		{
			snprintf(text_to_render, 1024, "Size: %zu bytes\n"
										   "Stacktrace:\n"
										   "%s", entry.size,
											entry.stacktrace);
		}

		size_t text_width =
			text_estimate_width(text_to_render, 30);

		if(text_width > max_width)
		{
			max_width = text_width;
		}
	}

	// make sure it's a size divisible by 2
	// otherwise ffmpeg can't make a video out of it
	if(max_width % 2 != 0)
	{
		max_width++;
	}

	return max_width;
}

int
main()
{
	struct Logs logs = logs_read("/tmp/log");
	printf("Number of entries: %zu\n", logs.entries_count);

	const char* output_path = "/home/adam/tmp/vis_out/";

	logs = logs_make_regions(logs);

	size_t stacktrace_width = stacktrace_max_width(logs);

	uint64_t addresses_sum = 0;

	for(size_t i = 0; i < logs.regions_count; i++)
	{
		addresses_sum += logs.regions[i].max_addr;
	}

	addresses_sum = nearest_square(addresses_sum);

	uint32_t dim = (uint64_t)sqrtf((double)addresses_sum);

	struct Image img = image_make(dim, dim, 3);

	for(size_t i = 0; i < logs.entries_count; i++)
	{
		struct LogEntry entry = logs.entries[i];
		uint32_t x, y;
		char path[MAX_PATH_LEN];

		mem_address_to_image_coord(entry.address_offset + entry.address, img.w, &x, &y);

		if(entry.size == deletion)
		{
			image_blacken_until_color_change(img, x, y);
		}
		else
		{
			struct Color c = color_randomize();
			img = image_set_color_until(img, x, y, c, entry.size);
		}

		struct Image resized = image_resize(img, 1000.0f / img.w);

		char text_to_render[1024];

		if(entry.size == deletion)
		{
			snprintf(text_to_render, 1024, "Stacktrace:\n"
										   "%s", entry.stacktrace);
		}
		else
		{
			snprintf(text_to_render, 1024, "Size: %zu bytes\n"
										   "Stacktrace:\n"
										   "%s", entry.size,
											entry.stacktrace);
		}

		struct Image side_image = image_make(stacktrace_width, resized.h, resized.c);

		struct Color c = {255, 255, 255, 255};
		side_image =
			text_render_to_image(text_to_render, 30, c,
								 0, 0, side_image);

		struct Image final = image_concatenate(resized, side_image);

		image_free(side_image);

		snprintf(path, MAX_PATH_LEN, "%s%05zu.png", output_path, i);
		image_write(final, path);
		image_free(final);
		image_free(resized);
	}

	wait(NULL);

	logs_free(logs);
	image_free(img);
}
